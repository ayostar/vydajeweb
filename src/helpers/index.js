import React from 'react';

export const categories = [
  'Jídlo/Nealko',
  'Alko',
  'Restaurace',
  'Byt',
  'Oblečení',
  'Cestování',
  'Vzdělávání',
  'Sport',
  'Půjčky',
  'Ostatní'
];

export const types = ['výdaj', 'příjem'];

export function generateCategory(transactions, category) {
  return Object.keys(transactions)
    .filter(item => transactions[item].category === category)
    .map(item => {
      const { type, amount } = transactions[item];
      if (type === 'výdaj') {
        return `-${amount}`;
      }
      return amount;
    })
    .reduce((a, b) => {
      return +a + +b;
    }, 0);
}

export function generateBalanceDay(transactions, today) {
  return Object.keys(transactions)
    .filter(item => transactions[item].date.slice(0, 2) === today)
    .map(item => {
      const { type, amount } = transactions[item];
      if (type === 'výdaj') {
        return `-${amount}`;
      }
      return 0;
    })
    .reduce((a, b) => {
      return +a + +b;
    }, 0);
}

export function generateBalanceMonth(transactions) {
  return Object.keys(transactions)
    .map(item => {
      const { type, amount } = transactions[item];
      if (type === 'výdaj') {
        return `-${amount}`;
      }
      return amount;
    })
    .reduce((a, b) => {
      return +a + +b;
    }, 0);
}

export function colorAmount(num) {
  return (
    <span className={num < 0 ? 'has-text-danger' : 'has-text-success'}>
      {num > 0 ? `+${num}` : num}
    </span>
  );
}
