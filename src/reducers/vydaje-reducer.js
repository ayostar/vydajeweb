const initialState = {
  balanceDay: 0,
  balanceMonth: 0,
  transactions: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case 'FETCH_TRANSACTIONS': {
      return { ...state, transactions: action.payload };
    }
    default: {
      return state;
    }
  }
}
