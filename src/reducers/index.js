import { combineReducers } from 'redux';
import vydajeReducer from './vydaje-reducer';

const rootReducer = combineReducers({
  vydaje: vydajeReducer
});

export default rootReducer;
