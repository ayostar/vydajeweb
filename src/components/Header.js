import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import Image from './common/Image';

class Header extends Component {
  render() {
    const { pathname } = this.props.location;
    return (
      <div className="container">
        <div className="landing-container">
          <Image containerCss="image is-64x64 landing-logo" src="../img/vydaje-logo-outlined.png" />
          <div className="title is-1 has-text-centered">
            Výdaje App
            {pathname === '/' ? (
              <Link to="/add" className="button is-primary add-button">
                Add new transaction
              </Link>
            ) : (
              <Link to="/" className="button is-primary add-button">
                Go Back
              </Link>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default Header;
