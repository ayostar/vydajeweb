import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Header from './Header';
import Landing from './Landing';
import Add from '../containers/Add';

const App = () => {
  return (
    <Router>
      <div>
        <Route path="*" component={Header} />
        <Route exact path="/" component={Landing} />
        <Route path="/add" component={Add} />
      </div>
    </Router>
  );
};

export default App;
