import React from 'react';
import { colorAmount } from '../helpers';

const Category = ({ title, value }) => {
  return (
    <div className="columns category">
      <div className="column is-half">
        <h2 className="is-size-3">{title}</h2>
      </div>
      <div className="column is-half">
        <h2 className="is-size-3 has-text-right">{colorAmount(value)}</h2>
      </div>
    </div>
  );
};

export default Category;
