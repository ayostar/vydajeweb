import React from 'react';

const Icon = ({ spanCss, iCss }) => {
  return (
    <span className={spanCss}>
      <i className={iCss} />
    </span>
  );
};

export default Icon;
