import React from 'react';

const Image = ({ containerCss, imgCss, src, alt }) => {
  return (
    <figure className={containerCss}>
      <img src={src} className={imgCss} alt={alt} />
    </figure>
  );
};

export default Image;
