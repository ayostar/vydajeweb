import React from 'react';

const Input = ({ label, name, type, value, onChange }) => {
  return (
    <div className="field is-horizontal">
      <div className="field-label is-normal is-large">
        <label className="label">{label}:</label>
      </div>
      <div className="field-body">
        <div className="control">
          <input
            name={name}
            type={type}
            className="input is-large"
            value={value}
            onChange={onChange}
          />
        </div>
      </div>
    </div>
  );
};

export default Input;
