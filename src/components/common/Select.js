import React from 'react';

const Select = ({ label, name, value, onChange, options }) => {
  return (
    <div className="field is-horizontal">
      <div className="field-label is-normal is-large">
        <label className="label">{label}:</label>
      </div>
      <div className="field-body">
        <div className="control">
          <div className="select is-large">
            <select name={name} value={value} onChange={onChange}>
              {options.map((item, index) => (
                <option key={index} value={item}>
                  {item}
                </option>
              ))}
            </select>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Select;
