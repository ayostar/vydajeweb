import React from 'react';

import Boxes from '../containers/Boxes';
import Transactions from '../containers/Transactions';
import Categories from '../containers/Categories';

const Landing = () => {
  return (
    <section className="section">
      <div className="container">
        <Boxes />
        <div className="columns">
          <div className="column is-half">
            <Categories />
          </div>
          <div className="column is-half">
            <Transactions />
          </div>
        </div>
      </div>
    </section>
  );
};

export default Landing;
