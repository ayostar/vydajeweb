import React, { Component } from 'react';
import { connect } from 'react-redux';

import Icon from '../components/common/Icon';
import Transaction from './Transaction';

class Transactions extends Component {
  renderTransactions = () => {
    const { transactions } = this.props;
    /* cant map Object so this is workaround */
    return Object.keys(transactions).map((item, index) => {
      const { type, category, amount, date } = transactions[item];
      return (
        <Transaction
          key={index}
          id={item}
          type={type}
          category={category}
          amount={amount}
          date={date}
        />
      );
    });
  };

  render() {
    return (
      <div className="transactions-container">
        <div className="card">
          <div className="card-header">
            <div className="card-header-title">
              <h1 className="is-size-4 has-text-white">
                Transactions
                <Icon spanCss="icon is-large" iCss="fa fa-exchange" />
              </h1>
            </div>
          </div>
          <div className="card-content">{this.renderTransactions()}</div>
        </div>
      </div>
    );
  }
}

function mapStateToProps({ vydaje }) {
  return { transactions: vydaje.transactions };
}

export default connect(mapStateToProps)(Transactions);
