import React, { Component } from 'react';
import { connect } from 'react-redux';
import { deleteTransaction } from '../actions';

import Icon from '../components/common/Icon';

class Transaction extends Component {
  render() {
    const { id, type, category, amount, date } = this.props;
    const year = date.slice(6, 10);
    const month = date.slice(3, 5);
    return (
      <div className="columns is-vcentered transaction">
        <div className="column is-2">
          {type === 'příjem' ? (
            <Icon spanCss="icon has-text-success is-large" iCss="fa fa-3x fa-arrow-down" />
          ) : (
            <Icon spanCss="icon has-text-danger is-large" iCss="fa fa-3x fa-arrow-up" />
          )}
        </div>
        <div className="column is-3">
          <h1 className="title is-3">{type === 'výdaj' ? `-${amount}` : amount}</h1>
        </div>
        <div className="column is-3">
          <h1 className="title is-3">
            <u>{category}</u>
          </h1>
        </div>
        <div className="column is-3">
          <h2 className="subtitle is-4">Date: {date}</h2>
        </div>
        <div className="column">
          <button
            className="button is-danger is-outlined"
            onClick={() => this.props.deleteTransaction(id, { year, month })}>
            <Icon spanCss="icon" iCss="fa fa-times" />
          </button>
        </div>
      </div>
    );
  }
}

export default connect(null, { deleteTransaction })(Transaction);
