import React, { Component } from 'react';
import { connect } from 'react-redux';
import { categories, generateCategory } from '../helpers';

import Icon from '../components/common/Icon';
import Category from '../components/Category';

class Categories extends Component {
  render() {
    const { transactions } = this.props;
    const values = categories.map((item, index) => {
      return generateCategory(transactions, categories[index]);
    });
    return (
      <div className="categories-container">
        <div className="card">
          <div className="card-header">
            <div className="card-header-title">
              <h1 className="is-size-4 has-text-white">
                Categories
                <Icon spanCss="icon is-large" iCss="fa fa-th-large" />
              </h1>
            </div>
          </div>
          <div className="card-content">
            {categories.map((item, i) => {
              return <Category key={i} title={item} value={values[i]} />;
            })}
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps({ vydaje }) {
  return {
    transactions: vydaje.transactions
  };
}

export default connect(mapStateToProps)(Categories);
