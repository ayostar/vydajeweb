import React, { Component } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { generateBalanceDay, generateBalanceMonth, colorAmount } from '../helpers';

import Image from '../components/common/Image';

class Boxes extends Component {
  render() {
    const { transactions } = this.props;
    const today = moment().format('DD');
    const balanceDay = generateBalanceDay(transactions, today);
    const balanceMonth = generateBalanceMonth(transactions);

    return (
      <div className="boxes-container">
        <div className="card">
          <h2 className="title is-2 has-text-centered">{moment().format('dddd')}</h2>
          <h2 className="subtitle is-4 has-text-centered boxes-subtitle">
            {moment().format('DD.MM', { trim: true })}
          </h2>
          <div className="card-content">
            <h1 className="title is-4 amount-container">
              <span className="subtitle is-1 card-balance">{colorAmount(balanceDay)}</span>
              <Image src="../img/mince.png" imgCss="image is-48x48 coin" />
            </h1>
          </div>
        </div>
        <div className="card">
          <h2 className="title is-2 has-text-centered">{moment().format('MMMM')}</h2>
          <h2 className="subtitle is-4 has-text-centered boxes-subtitle">
            {moment().format('YYYY')}
          </h2>
          <div className="card-content">
            <h1 className="title is-4 amount-container">
              <span className="subtitle is-1 card-balance">{colorAmount(balanceMonth)}</span>
              <Image src="../img/mince.png" imgCss="image is-48x48 coin" />
            </h1>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps({ vydaje }) {
  return {
    transactions: vydaje.transactions
  };
}

export default connect(mapStateToProps)(Boxes);
