import React, { Component } from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { setTransaction } from '../actions';
import { types, categories } from '../helpers';

import Input from '../components/common/Input';
import Select from '../components/common/Select';

class Add extends Component {
  state = {
    date: moment().format('DD.MM.YYYY'),
    type: types[0],
    amount: 0,
    category: categories[0],
    desc: ''
  };

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { date, type, category, amount, desc } = this.state;
    if (date !== '' && type !== '' && category !== '' && amount !== 0) {
      this.props.setTransaction({
        date,
        type,
        category: type !== 'příjem' ? category : null,
        amount,
        desc
      });
      this.setState({
        date: moment().format('DD.MM.YYYY'),
        type: types[0],
        amount: 0,
        category: categories[0],
        desc: ''
      });
    }
  };

  render() {
    const { amount, type, category, date, desc } = this.state;
    return (
      <section className="section">
        <div className="container">
          <div className="add-container card">
            <div className="card-content">
              <form className="form" onSubmit={this.handleSubmit}>
                <Input
                  label="Date"
                  name="date"
                  type="text"
                  value={date}
                  onChange={this.handleChange}
                />
                <Select
                  label="Type"
                  name="type"
                  value={type}
                  onChange={this.handleChange}
                  options={types}
                />
                <Input
                  label="Amount"
                  name="amount"
                  type="number"
                  value={amount}
                  onChange={this.handleChange}
                />
                {type !== 'příjem' && (
                  <Select
                    label="Category"
                    name="category"
                    value={category}
                    onChange={this.handleChange}
                    options={categories}
                  />
                )}
                <Input
                  label="Desc"
                  name="desc"
                  type="text"
                  value={desc}
                  onChange={this.handleChange}
                />
                {/* SUBMIT BUTTON */}
                <div className="field is-horizontal">
                  <div className="field-label is-large" />
                  <div className="field-body">
                    <div className="control">
                      <button className="button is-primary is-large" type="submit">
                        Submit
                      </button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default connect(null, { setTransaction })(Add);
