import fire from '../fire';
import moment from 'moment';

const thisYear = moment().format('YYYY');
const thisMonth = moment().format('MM');

export function requestTransactions() {
  return dispatch => {
    fire
      .database()
      .ref(`${thisYear}/${thisMonth}/`)
      .on('value', snap => {
        const array = snap.val() && snap.val().transactions;
        dispatch(fetchTransactions(array || []));
      });
  };
}
export function fetchTransactions(data) {
  return {
    type: 'FETCH_TRANSACTIONS',
    payload: data
  };
}
export function setTransaction(formData) {
  const { date, category, type, amount } = formData;
  const [month, year] = formData.date.slice(3).split('.');
  const ref = fire
    .database()
    .ref(`${year}/${month}/transactions/`)
    .push();
  ref.set({
    date,
    type,
    category,
    amount
  });
  return {
    type: 'SET_TRANSACTION',
    payload: formData
  };
}
export function deleteTransaction(id, date) {
  fire
    .database()
    .ref(`${date.year}/${date.month}/transactions/${id}`)
    .remove();
  return {
    type: 'DELETE_TRANSACTION'
  };
}
